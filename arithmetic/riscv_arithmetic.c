#include <stdio.h>
//#include "exception.h" //write exception.h
//#include "riscvGlobal.h"
#include <string.h>
//#include "abacus_printf.c"
// covering only arithmetic instructions for now
//initialization to all the registers

struct reg{
	long value;
	long address;
};

long PC=0;

long power(long base, long exponential)
{
	long result=1;
	long x;
for(x=1; x<=exponential; x++)
{
	result=result*base;
}
return result;
}

long riscv_lui(struct reg rd, long imm31_12)	
{
rd.value=(0xFFFFF000)&(imm31_12<<12);
PC= PC+4;
return rd.value;
}


long riscv_auipc(struct reg rd, long imm31_12)	
{
long PC1;
PC1= PC+ imm31_12;
rd.value= (0xFFFFF000)&(PC1<<12);
PC= PC+4;
return rd.value;
}

long riscv_addi(struct reg rs1, struct reg rd, long imm11_0)	
{
rd.value= rs1.value + imm11_0;
PC=PC+4;
return rd.value;
}

long riscv_slti(struct reg rs1, struct reg rd, long imm11_0)	
{   

	if(((rs1.value)&(0x80000000))==0x00000000)
	{
	printf("error\n");	
	return 1;
	}
	else
	{
		if(rs1.value<imm11_0)
		rd.value=1;
		else 
		rd.value=0;
		PC=PC+4;
		return rd.value;
	}

}

long riscv_sltiu(struct reg rs1, struct reg rd, long imm11_0)	
{
	if(((rs1.value)&(0x80000000))==0x80000000)
	{
	printf("error\n");	
	return 1;
	}
	else
	{
		if(rs1.value<imm11_0)
		rd.value=1;
		else 
		rd.value=0;
		PC=PC+4;
		return rd.value;
	}
}

long riscv_xori(struct reg rs1, struct reg rd, long imm11_0)	//5 bit rs1
{
	if((imm11_0)==-1)
		rd.value=0x0000001F-(rs1.value);
	else
		rd.value=((0x00000FFF-imm11_0)&(rs1.value))|((imm11_0) & (0x0000001F-rs1.value));
		PC=PC+4;
return rd.value;
}

long riscv_ori(struct reg rs1, struct reg rd, long imm11_0)	
{
	
	rd.value=(imm11_0 | rs1.value);
	PC=PC+4;
return rd.value;
}

long riscv_andi(struct reg rs1, struct reg rd, char imm11_0)	
{
	
	rd.value=(imm11_0 & rs1.value);
	PC=PC+4;
return rd.value;
}

long riscv_slli(struct reg rs1, struct reg rd, long shamt)	
{
rd.value= rs1.value << shamt;	
PC=PC+4;
return rd.value;
}
long riscv_srli(struct reg rs1, struct reg rd, long shamt)	
{
rd.value =rs1.value >> shamt;
PC=PC+4;	
return rd.value;
}

long riscv_srai(struct reg rs1, struct reg rd, long shamt)	//check this
{
	if(shamt==0)
		rd.value=rs1.value;
	else
	{
	rd.value= rs1.value/power(2,shamt);
	rd.value=(rs1.value&0x80000000) | rd.value;
	}
PC=PC+4;

return rd.value;
}

long riscv_add(struct reg rs1, struct reg rs2, struct reg rd)
{ 
rd.value = rs1.value + rs2.value;
PC=PC+4;
return rd.value;
}

long riscv_sub(struct reg rs1, struct reg rs2, struct reg rd)//overflows to be ignored
{
	if(rs1.value>rs2.value)
{rd.value = rs1.value - rs2.value;
long overflow=0;}
else
	{rd.value = rs2.value- rs1.value;
		long overflow=1;
	}
		PC=PC+4;	
return rd.value;
}

long riscv_sll(struct reg rs1, struct reg rs2, struct reg rd)
{
long shift= (rs2.value)&(0x0000001F);
rd.value=rs1.value << shift;
PC=PC+4;
return rd.value;
}

long riscv_slt(struct reg rs1, struct reg rs2, struct reg rd)
{
	if(((rs1.value)&(0x80000000))==0x00000000)
	{
	printf("error\n");	
	return 1;
	}
	else{
if(rs1.value<rs2.value)
	rd.value=1;	
else
	rd.value=0;
PC=PC+4;
return rd.value;
}
}

long riscv_sltu(struct reg rs1, struct reg rs2, struct reg rd)	
{ if(((rs1.value)&(0x80000000))==0x00000000)
	{
	printf("error\n");	
	return 1;
	}
	else{
if(rs1.value<rs2.value)
	rd.value=1;	
else
	rd.value=0;
PC=PC+4;
return rd.value;
}}
long riscv_xor(struct reg rs1, struct reg rs2, struct reg rd)	
{
	rd.value=((0x00000FFF-rs2.value)&(rs1.value))|((rs2.value) & (0x0000001F-rs1.value));
	PC=PC+4;
return rd.value;
}



long riscv_srl(struct reg rs1, struct reg rs2, struct reg rd)
{
long shift= (rs2.value)&(0x0000001F);
rd.value=rs1.value >> shift;
PC=PC+4;
return rd.value;
}

long riscv_sra(struct reg rs1, struct reg rs2, struct reg rd)
{
long shift= (rs2.value)&(0x0000001F);
rd.value= rs1.value/power(2,shift);
rd.value=(rs1.value&0x80000000) | rd.value;
PC=PC+4;
return rd.value;
}

long riscv_or(struct reg rs1, struct reg rs2, struct reg rd)	
	{
	rd.value=(rs2.value | rs1.value);
	PC=PC+4;
	return rd.value;
}

long riscv_and(struct reg rs1, struct reg rs2, struct reg rd)
{
	
	rd.value=(rs2.value | rs1.value);
	PC=PC+4;
	return rd.value;
}

long riscv_mul(struct reg rs1, struct reg rs2, struct reg rd)
{
rd.value=((rs1.value)*(rs2.value))&(0x0000FFFF);
PC=PC+4;
return rd.value;
}

long riscv_mulh(struct reg rs1, struct reg rs2, struct reg rd)	
{
	if((((rs1.value)&(0x80000000))==0x80000000)&&(((rs2.value)&(0x80000000))==0x80000000))
	{
rd.value=(((rs1.value)*(rs2.value))&(0xFFFF0000))>>16;
PC=PC+4;
return rd.value;
}
else
{
printf("error \n");
return 1;
}
}


long riscv_mulshu(struct reg rs1, struct reg rs2, struct reg rd)
{
	if((((rs1.value)&(0x80000000))==0x00000000)&&(((rs2.value)&(0x80000000))==0x00000000))
	{
rd.value=((rs1.value)*(rs2.value))&(0xFFFF0000))>>16;
PC=PC+4;
return rd.value;
}
else
{
printf("error \n");
return 1;
}
}

long riscv_mulhu(struct reg rs1, struct reg rs2, struct reg rd)
{
	if((((rs1.value)&(0x80000000))==0x00000000)&&(((rs2.value)&(0x80000000))==0x80000000))
	{
rd.value=((rs1.value)*(rs2.value))&(0xFFFF0000))>>16;
PC=PC+4;
return rd.value;
}
else
{
printf("error \n");
return 1;
}
}

long main()
{
 struct reg x0;
 x0.address=0x00000000;
 x0.value=0x00000000;

 struct reg x1;
 x1.address=0x00000020;
 x1.value=0x00000000;

 struct reg x2;
 x2.address=0x00000040;
 x2.value=0x00000000;

 struct reg x3;
 x3.address=0x00000060;
 x3.value=0x00000000;

 struct reg x4;
 x4.address=0x00000080;
 x4.value=0x00000000;
  
 struct reg x5;
 x5.address=0x00000100;
 x5.value=0x00000000;

struct reg x6;
 x6.address=0x00000120;
 x6.value=0x00000000;

struct reg x7;
 x7.address=0x00000140;
 x7.value=0x00000000;

struct reg x8;
 x8.address=0x00000160;
 x8.value=0xF0000000;

 struct reg x9;
 x9.address=0x00000180;
 x9.value=0x80000000;

struct reg x10;
 x10.address=0x00000200;
 x10.value=0x00000000;

 struct reg x11;
 x11.address=0x00000220;
 x11.value=0x00000000;

 struct reg x12;
 x12.address=0x00000240;
 x12.value=0x00000000;

struct reg x13;
 x13.address=0x00000260;
 x13.value=0x00000000;

 struct reg x14;
 x14.address=0x00000280;
 x14.value=0x00000000;

 struct reg x15;
 x15.address=0x00000300;
 x15.value=0x00000000;

 struct reg x16;
 x16.address=0x00000320;
 x16.value=0x00000000;

 struct reg x17;
 x17.address=0x00000340;
 x17.value=0x00000000;

 struct reg x18;
 x18.address=0x00000360;
 x18.value=0x00000000;

 struct reg x19;
 x19.address=0x00000380;
 x19.value=0x00000000;

 struct reg x20;
 x20.address=0x00000400;
 x20.value=0x00000000;

 struct reg x21;
 x21.address=0x00000420;
 x21.value=0x00000000;

 struct reg x22;
 x22.address=0x00000440;
 x22.value=0x00000000;

struct reg x23;
 x23.address=0x00000460;
 x23.value=0x00000000;

struct reg x24;
 x24.address=0x00000480;
 x24.value=0x00000000;

struct reg x25;
 x25.address=0x00000500;
 x25.value=0x00000000;

struct reg x26;
 x26.address=0x00000520;
 x26.value=0x00000000;

 struct reg x27;
 x27.address=0x00000540;
 x27.value=0x00000000;

 struct reg x28;
 x28.address=0x00000560;
 x28.value=0x00000000;

 struct reg x29;
 x29.address=0x00000580;
 x29.value=0x00000000;

 struct reg x30;
 x30.address=0x00000600;
 x30.value=0x00000000;

 struct reg x31;
 x31.address=0x00000620;
 x31.value=0x00000001;

x1.value= riscv_lui(x1,1);	
printf("%ld \n",x1.value);

x1.value= riscv_auipc(x1,1);


printf("%ld \n",x1.value);

x2.value =riscv_addi(x1,x2,12);
printf("%ld \n",x2.value);

x3.value=riscv_slti(x2, x3, 20492);
printf("%ld\n", x3.value );

x4.value=riscv_xori(x5, x4, 0x00000FFF);
printf("%ld\n", x4.value );

x6.value=riscv_andi(x4, x6, 0x00000011);
printf("%ld\n", x6.value );

x7.value=riscv_srai(x9, x7, 0x00000001);
printf("%ld\n", x7.value );

x7.value=0x00000002;
x8.value=riscv_slt(x31,x7,x8);
printf("%ld\n", x8.value);

x9.value=0x0000AFFF;
x10.value=riscv_mulh(x9,x7,x10);
printf("%ld\n", x10.value);

return 0;
}
  