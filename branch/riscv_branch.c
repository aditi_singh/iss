#include <stdio.h>
//#include "exception.h" //write exception.h
//#include "riscvGlobal.h"
#include <string.h>
//#include "abacus_printf.c"
// covering only arithmetic instructions for now
//initialization to all the registers

struct reg{
	long value;
	long address;
};

long PC=0;

long power(long base, long exponential)
{
	long result=1;
	long x;
for(x=1; x<=exponential; x++)
{
	result=result*base;
}
return result;
}

long riscv_jal(struct reg rd, long imm)	//imm_20|10:1|11|19:12
{
	//imm is a signed offset in multiples of 2 bytes, it is added to pc to form jump target addresses
		//JAL stores the address of the instruction following the jump (pc+4) into register rd. uses x1 as the return address register
PC= PC+4;
rd.value=PC+imm;
return rd.value;
}


long riscv_jalr(struct reg rd, struct reg rs1, long imm)	
{
	//The target address is obtained by adding the 12-bit signed I-immediate to the register rs1, then setting the least-significant bit
	//of the result to zero. The address of the instruction following the jump (pc+4) is written to register rd.
	PC=PC+4;
rd.value=(imm+rs1.value)&(0xFFFFFFF0);
return rd.value;
}

long riscv_beq(struct reg rs1, struct reg rs2, long imm)	
{
if(rs1.value==rs2.value)
	{
PC=PC+4;
return imm+PC;
}
else
PC=PC+4;
}

long riscv_bne(struct reg rs1, struct reg rs2, long imm)	
{
if(rs1.value==rs2.value)
PC=PC+4;
else
{
PC=PC+4;
return imm+PC;
}
}

long riscv_blt(struct reg rs1, struct reg rs2, long imm)	
{
		if(((rs1.value)&(0x80000000))==0x80000000)
{
if(rs1.value<rs2.value)
	{
PC=PC+4;
return imm+PC;
}
else
PC=PC+4;
}
}

long riscv_bltu(struct reg rs1, struct reg rs2, long imm)	
{
		if(((rs1.value)&(0x80000000))==0x00000000)
{
if(rs1.value<rs2.value)
	{
PC=PC+4;
return imm+PC;
}
else
PC=PC+4;
}
}

ong riscv_bge(struct reg rs1, struct reg rs2, long imm)	
{
		if(((rs1.value)&(0x80000000))==0x80000000)
{
if(rs1.value<rs2.value)
	{
PC=PC+4;
return imm+PC;
}
else
PC=PC+4;
}
}

long riscv_bgeu(struct reg rs1, struct reg rs2, long imm)	
{
		if(((rs1.value)&(0x80000000))==0x00000000)
{
if(rs1.value<rs2.value)
	{
PC=PC+4;
return imm+PC;
}
else
PC=PC+4;
}
}

long main()
{
 struct reg x0;
 x0.address=0x00000000;
 x0.value=0x00000000;

 struct reg x1;
 x1.address=0x00000020;
 x1.value=0x00000000;

 struct reg x2;
 x2.address=0x00000040;
 x2.value=0x00000000;

 struct reg x3;
 x3.address=0x00000060;
 x3.value=0x00000000;

 struct reg x4;
 x4.address=0x00000080;
 x4.value=0x00000000;
  
 struct reg x5;
 x5.address=0x00000100;
 x5.value=0x00000000;

struct reg x6;
 x6.address=0x00000120;
 x6.value=0x00000000;

struct reg x7;
 x7.address=0x00000140;
 x7.value=0x00000000;

struct reg x8;
 x8.address=0x00000160;
 x8.value=0xF0000000;

 struct reg x9;
 x9.address=0x00000180;
 x9.value=0x80000000;

struct reg x10;
 x10.address=0x00000200;
 x10.value=0x00000000;

 struct reg x11;
 x11.address=0x00000220;
 x11.value=0x00000000;

 struct reg x12;
 x12.address=0x00000240;
 x12.value=0x00000000;

struct reg x13;
 x13.address=0x00000260;
 x13.value=0x00000000;

 struct reg x14;
 x14.address=0x00000280;
 x14.value=0x00000000;

 struct reg x15;
 x15.address=0x00000300;
 x15.value=0x00000000;

 struct reg x16;
 x16.address=0x00000320;
 x16.value=0x00000000;

 struct reg x17;
 x17.address=0x00000340;
 x17.value=0x00000000;

 struct reg x18;
 x18.address=0x00000360;
 x18.value=0x00000000;

 struct reg x19;
 x19.address=0x00000380;
 x19.value=0x00000000;

 struct reg x20;
 x20.address=0x00000400;
 x20.value=0x00000000;

 struct reg x21;
 x21.address=0x00000420;
 x21.value=0x00000000;

 struct reg x22;
 x22.address=0x00000440;
 x22.value=0x00000000;

struct reg x23;
 x23.address=0x00000460;
 x23.value=0x00000000;

struct reg x24;
 x24.address=0x00000480;
 x24.value=0x00000000;

struct reg x25;
 x25.address=0x00000500;
 x25.value=0x00000000;

struct reg x26;
 x26.address=0x00000520;
 x26.value=0x00000000;

 struct reg x27;
 x27.address=0x00000540;
 x27.value=0x00000000;

 struct reg x28;
 x28.address=0x00000560;
 x28.value=0x00000000;

 struct reg x29;
 x29.address=0x00000580;
 x29.value=0x00000000;

 struct reg x30;
 x30.address=0x00000600;
 x30.value=0x00000000;

 struct reg x31;
 x31.address=0x00000620;
 x31.value=0x00000001;

return 0;
}
  